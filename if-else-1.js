"use strict";
var input = document.getElementsByClassName('input'),
	result = document.getElementById('result');


function sortString(str) {
	if (str == "") {
		return "";
	}

	var resultArr = [],
		numberInStr;

	str = str.split(" ");

	for (var i = 0, len = str.length; i < len; i++) {
		numberInStr = str[i].match(/\d/);
		resultArr[numberInStr] = str[i];
	}

	return resultArr.join(" ");
}

input = Array.from(input);
var h;
input.map(function (e, i, arr) {
	h = document.createElement("p"),
		h.innerText = sortString(e.innerText);
	result.appendChild(h);
});